const submit = document.getElementById("submit")
const firstName=document.getElementById("firstName")
const firstInput = document.getElementById("firstInput")
const lastName=document.getElementById("lastName")
const lastInput=document.getElementById("lastInput")
const submitted = document.getElementById("submitted")
const form = document.getElementById("form")
const fullName = document.getElementById("fullName")

submit.addEventListener("click", (event)=>{
    event.preventDefault()
    firstInput.innerText=""
    lastInput.innerText=""
    submitted.innerText=""
    fullName.innerText=""
    if(!firstName.value && !lastName.value){
    firstInput.innerText="First name required"
        lastInput.innerText="Last name required"
    }
    else if(!lastName.value){
        lastInput.innerText="Last name required"
    }
    else if(!firstName.value){
        firstInput.innerText="First name required"
    }
    else {
        submitted.innerText = "Submitted Successfully"
        fullName.innerText=firstName.value+" "+lastName.value
        form.reset()
    }

})




